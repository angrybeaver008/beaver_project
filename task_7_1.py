# def convent_1(sm=2.54):
#     inch = float(input('Vvesti: '))
#     result_1 = inch * sm
#     return result_1
#
#
# print('Результат в сантиметрах:', "%.3f" % convent_1(sm=2.54))
#
#
# def convert_2(inch=0.394):
#     sm = float(input('Введите значение в сантиметрах: '))
#     result_2 = sm * inch
#     return result_2
#
#
# print('Результат в дюймах:', "%.3f" % convert_2(inch=0.394))
#
#
# def convert_3(km=1.609):
#     miles = float(input('Введите значение в милях: '))
#     result_3 = miles * km
#     return result_3
#
#
# print('Результат километрах:', convert_3(km=1.609))
#
#
# def convert_4(miles=0.621):
#     km = float(input('Введите значение в километрах: '))
#     result_4 = km * miles
#     return result_4
#
#
# print('Результат в милях:', convert_4(miles=0.621))
#
#
# def convert_5(kg=0.453):
#     lb = float(input('Введите значение в фунтах: '))
#     result_5 = lb * kg
#     return result_5
#
#
# print('Результат в килограммах:', convert_5(kg=0.453))
#
#
# def convert_6(lb=2.204):
#     kg = float(input('Введите значение в килограммах: '))
#     result_6 = kg * lb
#     return result_6
#
#
# print('Результат в фунтах:', convert_6(lb=2.204))
#
#
# def convert_7(oz=28.35):
#     g = float(input('Введите значение в грммах: '))
#     result_7 = g * oz
#     return result_7
#
#
# print('Результат в граммах:', '%.3f' % convert_7(oz=28.35))
#
#
# def comvert_8(g=0.035):
#     oz = float(input('Введите значение в унциях: '))
#     result_8 = oz * g
#     return result_8
#
#
# print('Результат в унциях:', '%.3f' % comvert_8(g=0.035))
#
#
# def convert_9(litr=3.785):
#     gal = float(input('Введите значение в галлонах: '))
#     result_9 = gal * litr
#     return result_9
#
#
# print('Результат в литрах:', convert_9(litr=3.790))
#
#
# def convert_10(gal=0.264):
#     litr = float(input('Введите значение в литрах: '))
#     result_10 = litr * gal
#     return result_10
#
#
# print('Результат в галлонах:', convert_10(gal=0.264))
#
#
# def convert_11(litr=0.473):
#     pint = float(input('Введите значение в пинтах: '))
#     result_11 = pint * litr
#     return result_11
#
#
# print('Результат в литрах:', convert_11(litr=0.473))
#
#
# def convert_12(pint=2.113):
#     litr = float(input('Введите значение в литрах: '))
#     result_12 = litr * pint
#     return result_12
#
#
# print('Результат в пинтах:', convert_12(pint=2.113))

while True:
    znak = input('Выбор функции перевода:\n '
                 '1-Перевод дюймов в сантиметры;\n '
                 '2-Перевод сантиметров в дюймы;\n '
                 '3-Перевод миль в километры;\n '
                 '4-Перевод километров в мили;\n '
                 '5-Перевод фунтов в килограммы;\n '
                 '6-Перевод килограммов в фунты;\n '
                 '7-Перевод унций в граммы;\n '
                 '8-Перевод грамм в унции;\n '
                 '9-Перевод галлонов в литры;\n '
                 '10-Перевод литров в галлоны;\n '
                 '11-Перевод пинт в литры;\n '
                 '12-Перевод литров в пинты;\n '
                 '0-Выход из программы.\n'
                 'Сделай выбор: ')
    if znak == '0':
        break
    if znak in ('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'):
        n = float(input('Введите число: '))
        if znak == '1':
            def convent_1(sm=2.54):
                result_1 = n * sm
                return result_1


            print('Результат в сантиметрах:', "%.3f" % convent_1(sm=2.54))
        elif znak == '2':
            def convert_2(inch=0.394):
                result_2 = n * inch
                return result_2


            print('Результат в дюймах:', "%.3f" % convert_2(inch=0.394))
        elif znak == '3':
            def convert_3(km=1.609):
                result_3 = n * km
                return result_3


            print('Результат километрах:', convert_3(km=1.609))
        elif znak == '4':
            def convert_4(miles=0.621):
                result_4 = n * miles
                return result_4


            print('Результат в милях:', convert_4(miles=0.621))
        elif znak == '5':
            def convert_5(kg=0.453):
                result_5 = n * kg
                return result_5


            print('Результат в килограммах:', convert_5(kg=0.453))
        elif znak == '6':
            def convert_6(lb=2.204):
                result_6 = n * lb
                return result_6


            print('Результат в фунтах:', convert_6(lb=2.204))
        elif znak == '7':
            def convert_7(oz=28.35):
                result_7 = n * oz
                return result_7


            print('Результат в граммах:', '%.3f' % convert_7(oz=28.35))
        elif znak == '8':
            def comvert_8(g=0.035):
                result_8 = n * g
                return result_8


            print('Результат в унциях:', '%.3f' % comvert_8(g=0.035))
        elif znak == '9':
            def convert_9(litr=3.785):
                result_9 = n * litr
                return result_9


            print('Результат в литрах:', convert_9(litr=3.790))
        elif znak == '10':
            def convert_10(gal=0.264):
                result_10 = n * gal
                return result_10


            print('Результат в галлонах:', convert_10(gal=0.264))
        elif znak == '11':
            def convert_11(litr=0.473):
                result_11 = n * litr
                return result_11


            print('Результат в литрах:', convert_11(litr=0.473))
        elif znak == '12':
            def convert_12(pint=2.113):
                result_12 = n * pint
                return result_12


            print('Результат в пинтах:', convert_12(pint=2.113))

print('Нажмите "0" для выхода из программы')
while True:
    znak = input('Введите знак операции:')
    if znak == '0':
        break
    if znak in ('+', '-', '*', '/'):
        x = float(input('Введите X:'))
        y = float(input('Введите Y:'))
        if znak == '+':
            print('%.2f' % (x + y))
        elif znak == '-':
            print('%.2f' % (x - y))
        elif znak == '*':
            print('%.2f' % (x * y))
        elif znak == '/':
            if y != 0:
                print('%.2f' % (x / y))
            else:
                print('Деление на "0" запрещено')
    else:
        print('Неверный знак операции')
